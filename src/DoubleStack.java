
import java.util.Arrays;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class DoubleStack {

   private LinkedList<Double> magasin;
   public static void main (String[] argum) {
/*    DoubleStack proov1 = new DoubleStack();
      DoubleStack proov2 = new DoubleStack();
      proov1.magasin.push(1.);
      proov2.magasin.push(1.);
      System.out.println(proov1.equals(proov2));


//      proov.op("-");

      //DoubleStack.interpret("2. 5. -");*/
      System.out.println(DoubleStack.interpret("7 cc -"));


      // TODO!!! Your tests here!
   }

   DoubleStack() {
      magasin = new LinkedList<Double>();

      // TODO!!! Your constructor here!
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      // proovime new stak teha ja sellle kohe clonida objektid k�lge
      DoubleStack clone = new DoubleStack();
      clone.magasin = (LinkedList<Double>)magasin.clone();
      //new DoubleStack();
      return clone; // TODO!!! Your code here!
      // k�ivitab klassi DoubleStack ja loon uue listi, see j�rel kloonib andemd uuele listile
   }

   public boolean stEmpty() {
      if (magasin.isEmpty()) {
         return true;
      }else {
         return false;
      }

      // TODO!!! Your code here!
   }

   public void push (double a) {

      magasin.push(a);

      // TODO!!! Your code here!
   }

   public double pop() {
      //double temp = magasin.size();
      if (magasin.size() < 1) {
        throw new RuntimeException("magasiinis on liiga vahe elemente"); // TODO!!! Your code here!
      }
//      if (magasin.contains("\t")){
//         throw new RuntimeException("magasiinis sisaldab tab'e");
//      }

      else {
         return magasin.pop();
      }
   }// pop

   public void op (String s) {
      if(magasin.size() < 2)
      {
         throw new RuntimeException("magasiinis on liiga vahe elemente");
      }

      char tehe = s.charAt(0);
      double e2 = magasin.pop();
      double e1 = magasin.pop();
      //System.out.println("e1 - "+e1+"e2 - "+e2);
      if (tehe == '+'){
         push(e1+e2);
      }else if (tehe == '-') {
         push(e1-e2);
      }else if (tehe == '*') {
         push(e1*e2);
      }else if (tehe == '/') {
         push(e1/e2);
      }else {
         throw new RuntimeException("vale tehte element"+s);
      }
      // TODO!!!
      // koht kus k�sitakse tehte m�rki ja tehakse tehe listi esimesel ja teisel kohal oleva elemendiga
   }
  
   public double tos() {

      if(magasin.size() < 1)
      {
         throw new RuntimeException("magasiinis on liiga vahe elemente");
      }

      return magasin.peekFirst();
      // TODO!!! Your code here!
      // vaadata ule peek ja peekFirst juhtumitel
   }

   @Override
   public boolean equals (Object o) {


      DoubleStack ds = (DoubleStack)o;
      LinkedList<Double> temp = (LinkedList<Double>)ds.magasin; // temp listi, kus anname likedListi tyybi ja klassityybi (et o oleks meie klassi tyypi)
      if (temp.size() == magasin.size()){
         for (int i = 0; i < magasin.size(); i++) {
            if ((double)magasin.get(i)!= (double)temp.get(i)){ // peab kontrollima, et need ei v6rduks omavahel, muidu listi lopus ei saa true vaartust valjastada
               return false;
            }


         }
         return true;
      }

      return false; // TODO!!! Your code here!
   }

   @Override
   public String toString() {
      StringBuffer list = new StringBuffer();
      for (int i = (magasin.size()-1); i >=0 ; i--) {
         list.append(magasin.get(i));
         if (i>0){
            list.append(';');
         }

      }
      return list.toString();
      // TODO!!! Your code here!
      // muudame numrbid stingideks ja lisame ; vnende numbrite vahele
   }

   public static double interpret (String pol) {
      //http://crunchify.com/java-stringtokenizer-and-string-split-example/
      //pol.replaceAll("\t", ",");
      //System.out.println("++++++++++++++++++++++++++++++" + pol + "----------");

      if (pol == null || pol.length() == 0 ){
         throw new RuntimeException("valemikast on tyhi");
      }



      //pol.replaceAll("\\t", "\\s");
      StringTokenizer st = new StringTokenizer(pol);

      DoubleStack ds = new DoubleStack();
      int i = 0;
      int o = 0;
      //System.out.println("i: "+i+";   o:"+o);
      while (st.hasMoreElements()){
         String string1 = st.nextElement().toString();
        // System.out.println("kontroll --------------"+string1);
         try {
            Double el = Double.parseDouble(string1);
           //System.out.println("kontroll ++++++++++++++++++++"+el);
            ds.push(el);
            i++;
         }catch (NumberFormatException e){
            if (!string1.equals("-") && !string1.equals("+") && !string1.equals("/") && !string1.equals("*")){
               throw new RuntimeException("vale tehtem�rk" + pol);
            }else if (ds.magasin.size() < 2){ // kui eelnev on tyhi siis ta arvutama ei hakka
               throw new RuntimeException("liiga v�he elemente" + pol);
            }else if (ds.stEmpty()){
               throw new RuntimeException("avaldis tyhi");
            } else if (string1.contains("\t")){
               throw new RuntimeException("avaldis sisaldab tab'e"+pol);
            }else if (string1.contains("    "))
               throw new RuntimeException("avaldis sisaldab tyhikuid"+pol);

            ds.op(string1);
            o++;
         }

      }
//      if (pol.contains("\t")) {
//         throw new RuntimeException("teie valem sisaldab tab'e" + pol);
//      }
       int a = 0;
       for (int j = 0; j <pol.length() ; j++) {
           if (pol.charAt(j)== ' ') {
               a++;

           }
       }
       if (a== pol.length()){
           throw new RuntimeException("teie valem sisaldab ainult tyhikuid" + pol);
       }

       if (i-o != 1){
         throw new RuntimeException("elemente on rohkem kui tehtemärke"+pol);
      }
      return ds.pop(); // TODO!!! Your code here!
   }

}

